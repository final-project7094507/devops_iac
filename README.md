# Install k8s cluster with Kubespray on Yandex Cloud

## Install this tools before using
```
terraform, kubectl, helm, jq, pip3.
```
## Cloning Kubespray repo
```
$ git clone https://github.com/kubernetes-sigs/kubespray.git kubespray -b release-2.17  (you can take any branch)

$ nano kubespray/ansible.cfg  (and add timeout = 60 in section [defaults])  
```

## Install Kubespray requirements
```
$ sudo pip3 install -r kubespray/requirements.txt
```

## Set Terraform variables for Yandex Cloud
```
$ nano terraform/private.auto.tfvars
```

## Create cloud resources and install k8s cluster
```
$ bash cluster_install.sh
```
## Add static ip for your master and app vm from Yandex Cloud UI

## Add your project gitlab registration token 
```
$ nano srv/roles/gitlab-runner/vars/main.yml
```
## Run playbook for install runners and some programs 
```
$ ansible-playbook srv/common.yaml -i srv/hosts.ini
```
## Change our repo devops_pipeline version 1.5 for example
```
$ git tag -a v1.5 -m "my version 1.5"
$ git push origin v1.5
```
## Add Loki+Grafana for monitor the logs of our pods (just local without svc and public ip, but you can do it)
```
$ helm repo add grafana https://grafana.github.io/helm-charts
$ helm install loki grafana/loki-stack --namespace loki --create-namespace --set grafana.enabled=true
$ kubectl get secret --namespace loki loki-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ;
$ kubectl port-forward --namespace loki service/loki-grafana --address 0.0.0.0  3000:80
```
## Install Prometheus Stack via docker-compose on srv (add your tg credentials to docker-compose.yaml and your public app ip to prometheus.yml) 
```
$ docker-compose up -d
```

## Delete cloud resources
```
$ bash cluster_destroy.sh
```
