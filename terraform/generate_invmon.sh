#!/bin/bash

set -e

printf "[all]\n"

for num in 1
do
printf "srv   ansible_host="
terraform output -json instance_mon_public_ips | jq -j ".[$num-1]"
printf "   ip="
terraform output -json instance_mon_private_ips | jq -j ".[$num-1]"
printf "   ansible_user=ubuntu\n"
done

for num in 1
do
printf "app   ansible_host="
terraform output -json instance_workers_public_ips | jq -j ".[$num-1]"
printf "   ip="
terraform output -json instance_workers_private_ips | jq -j ".[$num-1]"
printf "   ansible_user=ubuntu\n"
done
printf "\n\n"


cat << EOF
[runners]
srv
app
EOF
