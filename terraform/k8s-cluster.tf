terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.84.0"
    }
  }
}

# Variables
variable "yc_token" {
  type = string
  description = "Yandex Cloud API key"
}

variable "yc_cloud_id" {
  type = string
  description = "Yandex Cloud id"
}

variable "yc_folder_id" {
  type = string
  description = "Yandex Cloud folder id"
}

# Provider
provider "yandex" {
  token     = var.yc_token
  cloud_id  = var.yc_cloud_id
  folder_id = var.yc_folder_id
}

# Network
resource "yandex_vpc_network" "k8s-network" {
  name = "k8s-network"
}

resource "yandex_vpc_subnet" "k8s-subnet" {
  name           = "k8s-subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.k8s-network.id
  v4_cidr_blocks = ["192.168.10.0/24"]
  depends_on = [
    yandex_vpc_network.k8s-network,
  ]
}

# Compute instance for masters
resource "yandex_compute_instance" "k8s-master" {
  name        = "master"
  hostname    = "master"
  depends_on = [
    yandex_vpc_network.k8s-network,
    yandex_vpc_subnet.k8s-subnet,
  ]
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd8vmcue7aajpmeo39kk" # Ubuntu 20.04 LTS
      size     = 20
      type     = "network-ssd"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.k8s-subnet.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

# Compute instance for worker
resource "yandex_compute_instance" "k8s-workers" {
  name        = "app"
  hostname    = "app"
  depends_on = [
    yandex_vpc_network.k8s-network,
    yandex_vpc_subnet.k8s-subnet,
  ]
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd8vmcue7aajpmeo39kk" # Ubuntu 20.04 LTS
      size     = 30
      type     = "network-hdd"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.k8s-subnet.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

# Compute instance for monitoring
resource "yandex_compute_instance" "k8s-mon" {
  name        = "srv"
  hostname    = "srv"
  depends_on = [
    yandex_vpc_network.k8s-network,
    yandex_vpc_subnet.k8s-subnet,
  ]
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd8vmcue7aajpmeo39kk" # Ubuntu 20.04 LTS
      size     = 30
      type     = "network-hdd"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.k8s-subnet.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}



# Output values
output "instance_masters_public_ips" {
  description = "Public IP addresses for master-nodes"
  value = yandex_compute_instance.k8s-master.*.network_interface.0.nat_ip_address
}

output "instance_masters_private_ips" {
  description = "Private IP addresses for master-nodes"
  value = yandex_compute_instance.k8s-master.*.network_interface.0.ip_address
}

output "instance_workers_public_ips" {
  description = "Public IP addresses for worker-nodes"
  value = yandex_compute_instance.k8s-workers.*.network_interface.0.nat_ip_address
}

output "instance_workers_private_ips" {
  description = "Private IP addresses for worker-nodes"
  value = yandex_compute_instance.k8s-workers.*.network_interface.0.ip_address
}

output "instance_mon_public_ips" {
  description = "Public IP addresses for monitoring"
  value = yandex_compute_instance.k8s-mon.*.network_interface.0.nat_ip_address
}

output "instance_mon_private_ips" {
  description = "Private IP addresses for monitoring"
  value = yandex_compute_instance.k8s-mon.*.network_interface.0.ip_address
}
