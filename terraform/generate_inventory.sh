#!/bin/bash

set -e

printf "[all]\n"

for num in 1
do
printf "master   ansible_host="
terraform output -json instance_masters_public_ips | jq -j ".[$num-1]"
printf "   ip="
terraform output -json instance_masters_private_ips | jq -j ".[$num-1]"
printf "   etcd_member_name=etcd\n"
done

for num in 1 
do
printf "app   ansible_host="
terraform output -json instance_workers_public_ips | jq -j ".[$num-1]"
printf "   ip="
terraform output -json instance_workers_private_ips | jq -j ".[$num-1]"
printf "\n"
done

printf "\n[all:vars]\n"
printf "ansible_user=ubuntu\n"
printf "supplementary_addresses_in_ssl_keys='"
terraform output -json instance_masters_public_ips | jq -cj
printf "'\n\n"

cat << EOF
[kube-master]
master

[etcd]
master

[kube-node]
app

[kube-worker]
app

[k8s-cluster:children]
kube-master
kube-node
EOF
